module Route exposing (Route(..), parseUrl)
import Route exposing (Route)
import Url exposing (Url)
import Url.Parser exposing (..)


type Route
    = NotFound
    | Posts


parseUrl : Url -> Route
parseUrl url =
    case parse matchRoute url of
        Just route ->
            route

        Nothing ->
            NotFound


matchRoute : Parser (Route -> a) a
matchRoute =
    oneOf
        [ map Posts top
        , map Posts (s "posts")
        ]

Route.Posts ->
    let
        ( pageModel, pageCmds ) =
            ListPosts.init
    in
    ( ListPage pageModel, Cmd.map ListPageMsg pageCmds )